    # imports
from bin import configs
from instapy import InstaPy
from instapy import smart_run

username, password = configs.get_insta_login_config()

session = InstaPy(username=username,
                  password=password,
                  headless_browser=False)

with smart_run(session):
    session.set_relationship_bounds(enabled=True,
                                    delimit_by_numbers=True,
                                    potency_ratio=-1.21,
                                    max_followers=4000,
                                    min_followers=45,
                                    min_following=100)

    #  Put all the friends you don't want to touch here
    session.set_dont_include(["sexualising",
                              "charfred_",
                              "executedmemes",
                              "painting_update",
                              "9gag",
                              "fuckjerry",
                              "taylorstitch",
                              "classicalaf",
                              "memesslatt",
                              "999chaz",
                              "mudameme",
                              "agirlwithajournal",
                              "classical_art_memes_official",
                              "kf",
                              "tumblermeme",
                              "subtleasiantrait",
                              "asianandprou",
                              "pubit",
                              "sarcasm_only",
                              "daqua",
                              "lma",
                              "thefatjewish",
                              "betche",
                              "thefunnyintrover",
                              "bit.h",
                              "beigecardiga",
                              "noguhide5050",
                              "Memerstein",
                              "eat_my_memeis"])
    session.unfollow_users(amount=800,
                           nonFollowers=True,
                           style="RANDOM",
                           unfollow_after=10,
                           sleep_delay=1)
