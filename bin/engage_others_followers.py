from instapy import InstaPy
from instapy import smart_run
from bin import configs
# get a session!
username, password = configs.get_insta_login_config()
session = InstaPy(username=username, password=password)

photo_comments = configs.get_insta_comments()

with smart_run(session):
    session.set_user_interact(amount=3,
                              randomize=True,
                              percentage=100,
                              media='Photo')
    session.set_relationship_bounds(enabled=True,
                                    potency_ratio=None,
                                    delimit_by_numbers=True,
                                    max_followers=3000,
                                    max_following=900,
                                    min_followers=50,
                                    min_following=50)
    session.set_blacklist(enabled=True, campaign='blacklist')
    session.set_simulation(enabled=False)
    session.set_do_like(enabled=True, percentage=100)
    session.set_ignore_users([])
    session.set_do_comment(enabled=True, percentage=35)
    # session.set_do_follow(enabled=True, percentage=25, times=1)
    session.set_comments(photo_comments)
    session.set_ignore_if_contains([])
    session.set_action_delays(enabled=True, like=40, comment=40)
    # """ Joining Engagement Pods...
    # """
    # session.join_pods()

    # activity
    session.interact_user_followers(['KFC', "thefatjewish"], amount=200)
