# imports
from bin import configs
from instapy import InstaPy
from instapy import smart_run

username, password = configs.get_insta_login_config()

# get an InstaPy session!
# set headless_browser=True to run InstaPy in the background
session = InstaPy(username=username,
                  password=password,
                  headless_browser=False)

with smart_run(session):
    """ Activity flow """
    session.set_relationship_bounds(enabled=True,
                                    delimit_by_numbers=True,
                                    potency_ratio=-1.21,
                                    max_followers=4000,
                                    min_followers=45,
                                    min_following=100)
    session.set_blacklist(enabled=True, campaign='blacklist')
    # session.set_skip_users(skip_private=False)
    """ Joining Engagement Pods...
    """
    """ Massive Follow of users followers (I suggest to follow not less than
    3500/4000 users for better results)...
    """
    session.set_do_like(enabled=True, percentage=100)
    session.set_do_comment(enabled=True, percentage=100)
    session.set_comments(configs.get_insta_comments(), media='Photo')
    session.set_comments(
        ['nice', ':smiling_face_with_sunglasses: :thumbsup:', 'lol'], media='Video')
    session.set_simulation(enabled=True)
    photo_comments = configs.get_insta_comments()
    session.set_comments(photo_comments, media='Photo')
    # session.join_pods()
    session.follow_user_followers(
        ['kfc', '9gag'],
        amount=230,
        randomize=True,
        sleep_delay=600
    )
