from content import RedditClient, TimeScope

reddit_client = RedditClient(
    subs=["wholesomememes", "starterpacks", "memeeconomy",
          "memes", "meirl", "i_irl", "comedycemetery",
          "OTmemes", "dankmemes", "memes",
          "bonehurtingjuice", "memeeconomy", "BikiniBottomTwitter",
          "meirl", "i_irl", "comedycemetery", "OTmemes", "memes", "dankmemes"],
    download_path_from_root=["content", "files"],
    time_scope=TimeScope.WEEK
)

reddit_client.get_content()
