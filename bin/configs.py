import os
import yaml
import typing


def get_insta_login_config() -> typing.Tuple[str, str]:
    with open(os.path.join(os.path.dirname(__file__), *["prod.yaml"]), 'r') as stream:
        try:
            configs = yaml.load(stream)
            return configs['insta_username'], configs['insta_password']
        except yaml.YAMLError as exc:
            print(exc)
            raise Exception("Cannot load instagram config")


def get_insta_comments() -> typing.List[str]:
    return [
        'Really neat moment @{}',
        'Ninja @{}, just incredible. Keep it up.',
        'Your feed is :thumbsup: :scream:',
        'Just incredible :open_mouth:',
        'Not bad, @{} keep it up!',
        'Love your posts @{}',
        'Looks awesome @{}',
        'Getting inspired by you @{}',
        ':raised_hands: Yes!',
        'I can feel your passion @{} ',
    ]


def get_twitter_pupper_configs() -> dict:
    with open(os.path.join(os.path.dirname(__file__), *["prod.yaml"]), 'r') as stream:
        try:
            niche_dictionary = yaml.load(stream)['twitter']["niche"]
            return _load_twitter_niche_configs(niche_dictionary, "puppers")
        except yaml.YAMLError as exc:
            print(exc)
            raise Exception("Cannot load twitter config")


def get_twitter_kitty_configs() -> dict:
    with open(os.path.join(os.path.dirname(__file__), *["prod.yaml"]), 'r') as stream:
        try:
            niche_dictionary = yaml.load(stream)['twitter']["niche"]
            return _load_twitter_niche_configs(niche_dictionary, "kittens")
        except yaml.YAMLError as exc:
            print(exc)
            raise Exception("Cannot load twitter config")


def _load_twitter_niche_configs(config:dict, niche:str):
    niche_config = config[niche]
    return {
        'user': niche_config['user'],
        'password': niche_config['password'],
        'api_key': niche_config['api_key'],
        'api_secret': niche_config['api_secret'],
        'access_token': niche_config['access_token'],
        'access_secret': niche_config['access_secret']
    }
