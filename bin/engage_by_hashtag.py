from instapy import InstaPy
from instapy import smart_run
from bin import configs

username, password = configs.get_insta_login_config()

# noinspection PyUnboundLocalVariable
session = InstaPy(username=username,
                  password=password)

with smart_run(session):
    """ Start of parameter setting """
    session.set_delimit_liking(enabled=True, max=150, min=0)  # no like if a post already has more than 150 likes
    session.set_delimit_commenting(enabled=True, max=10, min=0)  # no comment if a post already has more than 4 comments

    """
        Relax (potency ratio, max_followers)
              (-0.50,2000) respectively.
    """
    session.set_relationship_bounds(enabled=True,
                                    potency_ratio=-0.50,
                                    delimit_by_numbers=True,
                                    max_followers=2000,
                                    max_following=3500,
                                    min_followers=25,
                                    min_following=25)
    session.set_do_comment(True, percentage=20)
    session.set_do_follow(enabled=True, percentage=20, times=2)
    session.set_comments(configs.get_insta_comments())
    """ Joining Engagement Pods...
    """
    session.join_pods()
    session.set_sleep_reduce(200)

    """ 
        Get the list of non-followers
        I duplicated unfollow_users() to see a list of non-followers which I 
        run once in a while when I time
        to review the list
    """
    # session.just_get_nonfollowers()

    # my account is small at the moment, so I keep smaller upper threshold
    session.set_quota_supervisor(enabled=True,
                                 sleep_after=["likes", "comments_d", "follows",
                                              "unfollows", "server_calls_h"],
                                 sleepyhead=True, stochastic_flow=True,
                                 notify_me=True,
                                 peak_likes=(100, 700),
                                 peak_comments=(25, 200),
                                 peak_follows=(48, 125),
                                 peak_unfollows=(35, 400),
                                 peak_server_calls=(None, 3000))

    # Like by tags
    """ I mostly use like by tags. I used to use a small list of targeted 
    tags with a big 'amount' like 300
        But that resulted in lots of "insufficient links" messages. So I 
        started using a huge list of tags with
        'amount' set to something small like 50. Probably this is not the 
        best way to deal with "insufficient links"
        message. But I feel it is a quick work around.
    """

    session.like_by_tags(['dankmemes', 'pickachumemes', 'gotmemes'],
                         amount=120)

    session.follow_user_followers(['romain.grg', 'memescentralbro', 'thebitterbanker'],
                                  amount=300,
                                  randomize=False, interact=True)
    session.unfollow_users(amount=300, InstapyFollowed=(True, "nonfollowers"),
                           style="FIFO",
                           unfollow_after=4 * 60 * 60, sleep_delay=601)
