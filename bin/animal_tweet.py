import sys
import os
sys.path.append('../')

from content.reddit.client import RedditClient, TimeScope
from content.twitter.client import TwitterClient
from content.twitter.utils import get_animal_medias, delete_animal_medias

niche = sys.argv[1]
niche_path = ["..", "content", "twitter", "images", niche]
subs = ["rarepuppers"] if niche == "puppers" else ["kittens"]


reddit_client = RedditClient(subs=subs,
                             download_path_from_root=niche_path,
                             time_scope=TimeScope.DAY)
reddit_client.get_content()

twitter_client = TwitterClient(niche)
animal_medias = get_animal_medias(niche)
twitter_client.tweet_image(animal_medias)

delete_animal_medias(niche)
