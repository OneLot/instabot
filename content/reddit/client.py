from enum import Enum
from typing import List, Tuple
from content.downloader.client import DownloadClient
from http import cookiejar
import bs4
import requests
import json


class CookieBlockAll(cookiejar.CookiePolicy):
    return_ok = set_ok = domain_return_ok = path_return_ok = lambda self, *args, **kwargs: False
    netscape = True
    rfc2965 = hide_cookie2 = False


class TimeScope(Enum):
    DAY = "day"
    WEEK = "week"
    MONTH = "month"
    YEAR = "year"
    ALL = "all"


class RedditClient(object):
    _user_agent = "User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_3) " \
                  "AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.89 Safari/537.36"

    def __init__(self,
                 subs:List[str],
                 download_path_from_root:List[str],
                 time_scope:TimeScope):
        self.client = requests.Session()
        self.client.cookies.set_policy(CookieBlockAll)
        self.client.headers = {
            'user-agent': self._user_agent,
            'X-Requested-With': 'XMLHttpRequest'
        }
        self.subs = subs
        self.time_scope = time_scope
        self._downloader = DownloadClient(download_path_from_root)
        self.content_to_poach: List[Tuple[str, str]] = []
        self._set_content_to_poach()

    def _set_content_to_poach(self):
        for sub in self.subs:
            dank_memes_r = self.client.get(url="https://www.reddit.com/r/{0}/top/?t={1}".format(sub, self.time_scope.name))
            soup = bs4.BeautifulSoup(dank_memes_r.content, 'lxml')
            memes = [meme.attrs for meme in (soup.findAll('img', {'alt':'Post image'}))
                     if meme.attrs['src'].startswith('https://preview')]
            count = 1
            for post in memes:
                self.add_content(post, count, sub)
                count += 1

    def add_content(self, meme_dico: dict, count: int, sub: str):
        try:
            image_url = meme_dico['src']
            name = sub + '_' + count.__str__()
            self.content_to_poach.append((name, image_url))
            print(f"retrieved {name}")
        except Exception as e:
            print("Error", e);pass

    def get_content(self):
        self._downloader.download_files(source=self.content_to_poach)
