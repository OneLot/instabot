import os
import glob
import random
from typing import List, Tuple


class AnimalMedia(object):
    def __init__(self,
                 caption_without_hash,
                 caption,
                 file_path,
                 niche):
        self.caption_without_hash = caption_without_hash
        self.caption = caption
        self.file_path = file_path
        self.niche = niche

    def __repr__(self):
        return self.caption


def get_animal_medias(niche: str) -> List[AnimalMedia]:
    niche_img_dir = __get_niche_dir(niche)
    images_dir = glob.glob(str(niche_img_dir) + '/*.jpg')
    hashes_to_append = __append_doggo_hashes__() if niche == "puppers" else __append_kitten_hashes__()
    return [
        AnimalMedia(
            caption_without_hash=__file_path_to_name_and_caption(file_path, hashes_to_append)[0].replace("reddit","twitter").replace("Reddit","Twitter"),
            caption=__file_path_to_name_and_caption(file_path, hashes_to_append)[1],
            file_path=file_path,
            niche=niche
        ) for file_path in images_dir
    ]


def delete_animal_medias(niche: str):
    files = glob.glob(__get_niche_dir(niche) + '/*')
    for f in files:
        os.remove(f)


def __get_niche_dir(niche: str):
    return os.path.abspath(os.path.join(os.getcwd(), *['..','content', 'twitter', 'images', niche]))


def __append_doggo_hashes__():
    puppy_hashtags = ["#puppyoftheday", "#puppylove", "#doglovers", "#dogs",
                      "#doglife", "#doggo", "#puppy", "#puppies", "#awwww", "#doglovers",
                      "#dailymotivation", "#dogsoftwitter", "#pupbox", "#pup", "#outstanding"]
    random.shuffle(puppy_hashtags)
    return "\n" + " ".join(puppy_hashtags[:3])


def __append_kitten_hashes__():
    kitten_hashtags = ["#kittens", "#kittenlove", "#catlovers", "#cats", "#kittenlife",
                       "#cute", "#happy", "#kitty", "#awwww", "#catsoftwitter", "#caturday"]
    random.shuffle(kitten_hashtags)
    return "\n" + " ".join(kitten_hashtags[:3])


def __file_path_to_name_and_caption(file_path: str, hashes_to_append: str) -> Tuple[str, str]:
    return (file_path.split('/')[-1].replace('_', ' ').replace('.jpg', '').capitalize(),
            file_path.split('/')[-1].replace('_', ' ').replace('.jpg', hashes_to_append).capitalize())
