import tweepy
import content.twitter.model as model
from tweepy.models import Media
from typing import List, Tuple
from bin.configs import get_twitter_pupper_configs, get_twitter_kitty_configs
from content.twitter.utils import AnimalMedia


class TwitterClient(object):

    def __init__(self, niche: str):
        self.niche = niche
        self.configs = get_twitter_pupper_configs() if self.niche == "puppers" else get_twitter_kitty_configs()
        self.auth = tweepy.OAuthHandler(
            consumer_key=self.configs['api_key'],
            consumer_secret=self.configs['api_secret']
        )
        self.auth.set_access_token(
            key=self.configs['access_token'],
            secret=self.configs['access_secret']
        )
        self.api = tweepy.API(self.auth)

    def tweet_image(self, medias: List[AnimalMedia]):
        print("medias tweet image input = {0}".format(len(medias)))
        media_to_tweet = model.animal_media_not_persist_yet(medias, self.niche)
        to_tweet: Tuple[AnimalMedia, Media] = self._single_upload(media_to_tweet)
        caption = to_tweet[0].caption
        caption_without_hash = to_tweet[0].caption_without_hash
        media_ids = [to_tweet[1].media_id]
        try:
            self.api.update_status(
                status=caption,
                media_ids=media_ids)
            print(media_to_tweet.niche)
            if media_to_tweet.niche == "puppers":
                print("block1")
                o = model.Puppers(name=caption_without_hash, twitter_id=media_ids[0]); o.add()
            else:
                print("block2")
                o = model.Kitties(name=caption_without_hash, twitter_id=media_ids[0]); o.add()
        except Exception as e:
            print('[error] cannot tweet', e)

    def _single_upload(self, animal_media: AnimalMedia):
        try:
            return animal_media, self.api.media_upload(filename=animal_media.file_path)
        except Exception as e:
            print('[error] cannot upload', e)
            pass
