from typing import List
from sqlalchemy import create_engine, MetaData
from sqlalchemy import Column, Integer, String, BigInteger
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from content.twitter.utils import AnimalMedia
import os

Base = declarative_base()


class Puppers(Base):
    __tablename__ = "puppers"

    id = Column(Integer, primary_key=True)
    name = Column(String)
    twtr_id = Column(BigInteger)

    def __init__(self, name: str, twitter_id: int):
        self.name = name
        self.twtr_id = twitter_id

    def add(self):
        session.add(self)
        session.commit()


class Kitties(Base):
    __tablename__ = "kitties"

    id = Column(Integer, primary_key=True)
    name = Column(String)
    twtr_id = Column(BigInteger)

    def __init__(self, name: str, twitter_id: int):
        self.name = name
        self.twtr_id = twitter_id

    def add(self):
        session.add(self)
        session.commit()


db_path = os.path.abspath(os.path.join(os.getcwd(), *["..", "content", "twitter", "media.db"]))
print(db_path)
engine = create_engine('sqlite:///{0}'.format(db_path), echo=True)
Base.metadata.create_all(engine)
Session = sessionmaker(bind=engine)
session = Session()
metaData = MetaData()


def animal_media_not_persist_yet(animal_medias: List[AnimalMedia], niche: str) -> AnimalMedia:
    all_persisted = _get_all_puppers_name() if niche == "puppers" else _get_all_kitties_name()
    print("all persisted :{0}  animal medias :{1} ".format(len(all_persisted),len(animal_medias)))
    return list(filter(lambda it: it.caption_without_hash not in all_persisted, animal_medias))[0]


def _get_all_puppers_name() -> List[Puppers]:
    return list(map(lambda p: p.name, session.query(Puppers).all()))


def _get_all_kitties_name() -> List[Kitties]:
    return list(map(lambda k: k.name, session.query(Kitties).all()))
