import urllib.request
from moviepy import video
from moviepy.editor import *
import os

url = "https://v16.tiktokcdn.com/c99bbde754d7649ab86a865a1f8fa275/5da383ed/video/n/v0102/91e74f9083b644d4a68c383b8c208253/?a=1180&br=864&cr=0&cs=0&dr=3&ds=3&er=&l=20191013140700010115069034045054E7&lr=tiktok&rc=MzV1czZwdnA6cDMzNDgzM0ApODllODdoZjs2N2U0Z2hoNGdiNGg0aG5nLjFfLS0xLzRzczFjYTRfXzJhNjJjLi4uMC46Yw%3D%3D"
name = "mariokart"
name = name + ".mp4"



try:
    print("Downloading starts...\n")
    urllib.request.urlretrieve(url, name)
    print("Download completed..!!")
except Exception as e:
    print(e)

clip: VideoFileClip = VideoFileClip(name)
tik_tok_logo_bounds = 130
edited_clip: VideoFileClip = video.fx.all.crop(clip, y1=tik_tok_logo_bounds)
edited_clip = video.fx.all.crop(edited_clip, y2=edited_clip.h - tik_tok_logo_bounds)
edited_clip.write_videofile("edit_" + name,
                            codec='libx264',
                            audio_codec='aac',
                            temp_audiofile='temp-audio.m4a',
                            remove_temp=True)

os.remove(name)
print('hi')
