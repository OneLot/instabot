import urllib.request
from typing import Tuple, List
import requests
import os


class DownloadClient(object):
    def __init__(self, path_from_root: List[str]):
        self.client = requests.Session
        self.path_from_root = path_from_root

    def download_files(self, source: List[Tuple[str, str]]):

        for s in source:
            self.__download_file(url=s[1],
                                 name=os.path.abspath(
                                     os.path.join(os.getcwd(), *self.path_from_root+[s[0] + '.jpg'])))

    @staticmethod
    def __download_file(url: str, name: str):
        try:
            print("Downloading " + url + "...\n")
            urllib.request.urlretrieve(url, name)
            print("Download completed..!!")
        except Exception as e:
            print(e)
