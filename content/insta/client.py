import time
from typing import List
from http import cookiejar
import bs4
import requests


class CookieBlockAll(cookiejar.CookiePolicy):
    return_ok = set_ok = domain_return_ok = path_return_ok = lambda self, *args, **kwargs: False
    netscape = True
    rfc2965 = hide_cookie2 = False


class InstaClient(object):
    def __init__(self):
        self.client = requests.Session()
        self.client.cookies.set_policy(CookieBlockAll)
        self.client.headers = {
            'authority': 'www.instagram.com',
            'upgrade-insecure-requests': '1',
            'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) '
                          'AppleWebKit/537.36 (KHTML, like Gecko) '
                          'Chrome/74.0.3729.169 Safari/537.36',
            'accept': 'text/html,application/xhtml+xml,'
                      'application/xml;q=0.9,'
                      'image/webp,image/apng,*/*;q=0.8,'
                      'application/signed-exchange;v=b3',
            'accept-encoding': 'gzip, deflate, br',
            'accept-language': 'en-US,en;q=0.9,fr;q=0.8'
        }
        self.client.cookies.set_policy(CookieBlockAll)

    def get_hashtag_count(self, hashtag_url: str) -> float:
        try:
            time.sleep(2.1)
            response = self.client.get(url='https://www.instagram.com/explore/tags/{}/'.format(hashtag_url))
            soup = bs4.BeautifulSoup(response.content, 'lxml')
            hashtag_content: str \
                = (soup.find_all('meta', {'name': 'description'})[0]
                .attrs['content']
                .split(' ')[0])
            print('retrieved {} for hash {}'.format(hashtag_content,
                                                    hashtag_url))
            return self.text_to_num(hashtag_content)
        except Exception as e:
            print(e)
            return 0.0

    def read_hashfile_print_hash_and_count(self):
        hashtags = []
        with open("hashtag_in.txt", 'r') as f:
            for line in f:
                if '\n' == line:
                    break
                else:
                    hash_tag = (line[1:][:-2])
                    hashtags.append(hash_tag)
        hash_and_count_delimited: List[str] = list(map(lambda h: '|'.join((h, str(self.get_hashtag_count(h)))), hashtags))
        [print(elem) for elem in hash_and_count_delimited]

    @staticmethod
    def text_to_num(text: str) -> float:
        d = {
            'K': 3,
            'M': 6,
            'B': 9
        }
        if text[-1].upper() in d:
            num, magnitude = text[:-1], text[-1].upper()
            return float(num) * 10 ** d[magnitude]
        else:
            return float(text.replace(',', ''))


c = InstaClient()
c.read_hashfile_print_hash_and_count()
